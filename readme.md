cls
# Webplatform

## Pré-requis 

- Redis 
- MongoDB

## Installation 

```
npm install
npm start 
```

## Roadmap

##### Lundi 19/12/2016
- [x] Gestion des données utilisateur(s). (Script de base ok...)
- Authentification local.
  - [x] login
  - [x] register
  - [ ] envoie de mail, Ok mais ne fonctionne pas, en attente d'un serveur SMTP (@Nopoza :3 ?)
- Tags et design de base (amis uniquement). 