/// <reference path='../interfaces.d.ts' />

// Import packages
import * as http from "http";
import * as path from "path";
import * as koa from "koa";
import * as bodyparser from "koa-bodyparser";
import * as router from "koa-router";
import * as passport from "koa-passport";
import * as mongodb from "mongodb";
import * as socketIO from "socket.io";
import * as chalk from "chalk";

// Import addons
import { Cluster } from "./addons/cluster";
import { Config } from "./addons/config";
import { Koa_Session, KoaStore_Redis } from "./addons/session";
import { Loader } from "./addons/loader";
import { KoaNunjucks } from "./addons/nunjucks";

// Import strategies
import { FacebookStrategy } from "./strategies/facebook";
import { TwitterStrategy } from "./strategies/twitter";

const helmet: any = require('koa-helmet');

// Start server
(async () => {
    const configuration: ConfigContent = await Config.init();
    new Cluster(slave).start(1);
})();

async function slave() : Promise<void> {

    Config.load();
    const configuration: ConfigContent = Config.config;

    mongodb.MongoClient.connect('mongodb://'+configuration.database, async (err: mongodb.MongoError,db: mongodb.Db) => {
        if(err) {
            console.log(chalk.red(err.toString()));
        }
        else {
            console.log(chalk.green.bold('[MongoDB]')+` Connected with url ${chalk.yellow.bold(configuration.database)}`);
        }
        const app: koa = new koa();

        app.use( bodyparser() );
        app.use( helmet() );

        // Template engine
        app.use( KoaNunjucks( path.join(__dirname,"../views") , {
            cache: false
        } ) );

        // Session 
        const AppStore: KoaStore_Redis = new KoaStore_Redis(configuration.redis);
        app.use( Koa_Session( {
            store: AppStore,
            maxAge: 86400000
        } ) ); 

        // Passport JS
        //app.use( passport.initialize() ); 
        //app.use( passport.session() ); 

        app.use( async (ctx: any,next) => {
            ctx.db = db;
            await next();
        }); 

        const routes: string[] = await Loader( path.join(__dirname,'routes') );
        routes.forEach( (jsfile: string) => {
            console.log(`Load route : ${chalk.yellow.bold(jsfile)}`);
            const file: router = require(jsfile);
            app.use( file.routes() ).use( file.allowedMethods() ); 
        });

        // Routing
        //passport.use(FacebookStrategy(conn));
        //passport.use(TwitterStrategy(conn));

        const server: http.Server = http.createServer(app.callback());
        const io: SocketIO.Server = socketIO(server);

        io.on('connection', async (socket: SocketIO.Socket) : Promise<void> => {

            console.time(chalk.cyan.bold('socket_perf'));
            function inviteMode() {
                console.log(chalk.green.bold('[Socket] ')+chalk.yellow.bold('Invite mode triggered')); 
                console.timeEnd(chalk.cyan.bold('socket_perf'));
            }

            console.log(chalk.green.bold('[Socket]')+' User connected with id => '+chalk.yellow.bold(`${socket.id}`));
            if(socket.request.headers.cookie !== undefined) {
                const userSess: Session = await AppStore.socketSession(socket.request.headers.cookie);
                if(userSess !== undefined) {
                    console.log(userSess);
                    console.timeEnd(chalk.cyan.bold('socket_perf'));
                }
                else {
                    inviteMode();
                }
            }
            else {
                inviteMode();
            }
        }); 

        // Listen app on port!
        console.log(`HTTP Server listen on ${chalk.yellow.bold(configuration.httpPort.toString())}`);
        server.listen(configuration.httpPort);
    });

    // Close & Exception handling
    const closeHandler : () => void = () => {
        process.exit();
    }
    process.on('exit',closeHandler);
    process.on('SIGINT',closeHandler);
    process.on('uncaughtException',closeHandler);
    
}