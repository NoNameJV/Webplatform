import * as process from "process";

export function log(...args) : void {
    console.log(`#${process.pid} >`, ...args);
}