import * as cluster from "cluster";
import * as os from "os";
import * as chalk from "chalk";

import { log } from "./logger";

export class Cluster {

    private masterWork: () => void;
    private slaveWork: () => void;
    private nbForks: number;

    constructor(slaveWork: () => void, masterWork?: () => void, nbForks: number = os.cpus().length) {
        this.slaveWork = slaveWork;
        this.masterWork = masterWork || (() => { });
        this.nbForks = nbForks || os.cpus().length;

        cluster.on("exit", (worker: cluster.Worker, code: any) : void => {
            log(`Worker #${chalk.green.bold(worker.process.pid.toString())} died`);
        });
    }

    public start(nbForks?: number) : void  {
        if (cluster.isMaster) {
            const forkNB : number = nbForks || this.nbForks;
            log(`Clustering on ${chalk.green.bold(forkNB.toString())} cpus...`);
            this.fork(nbForks);
            this.masterWork();
        } else this.slaveWork();
    }

    public setNbForks(newNbForks: number, killSurplus: boolean = false) : void {
        if (!cluster.isMaster) throw new Error("Can't edit forks: you're a slave !");

        if (killSurplus && this.nbForks > newNbForks)
            for (let i: number = this.nbForks - 1; i >= newNbForks; i--)
                cluster.workers[i].kill();

        else if (this.nbForks < newNbForks)
            this.fork(newNbForks);

        this.nbForks = newNbForks;
    }

    private fork(newNbForks?: number) : void {
        for (let i: number = Object.keys(cluster.workers).length; i < (newNbForks || this.nbForks); i++) {
            const worker: cluster.Worker = cluster.fork();
            log(`New worker with pid => ${chalk.green.bold(worker.process.pid.toString())}`);
        }
    }
}
