import * as uid from "uid-safe";
import * as cookies from "cookies";
import * as cookie from "cookie";
import * as Redis from "ioredis";
import * as koa from "koa";
import * as chalk from "chalk";

export abstract class Store {

    abstract get(sid: string | number); 
    abstract set(session: any, opts: any);
    abstract destroy(sid: string | number);
}

export class LocalStore extends Store {

    private session;

    constructor() {
        super();
        this.session = {};
    }

    decode(str: string) {
        if(!str) return "";
        let session: string = "";
        try{
            session = new Buffer(str, "base64").toString();
        } 
        catch(e) {}
        return JSON.parse(session);
    }

    encode(obj: any) : string {
        return new Buffer(obj).toString("base64");
    }

    get(sid: string | number) {
        return this.decode(this.session[sid]);
    }

    generateUID(length: number) {
        return uid.sync(length);
    }

    set(session: any, opts: any) {
        opts = opts || {};
        let sid = opts.sid;
        if(!sid) sid = this.generateUID(24);
        this.session[sid] = this.encode(JSON.stringify(session));
        return sid;
    }

    destroy(sid: string | number) : void {
        delete this.session[sid];
    }
}

export interface KoaStore_Options {
    ip?: string;
    port?: number;
}

export class KoaStore_Redis extends LocalStore {

    private redis: Redis.Redis;

    constructor(opts: KoaStore_Options) {
        super();
        console.log(chalk.green.bold('[Redis]')+` Connected with port ${chalk.yellow.bold(opts.port.toString())} and ip ${chalk.yellow.bold(opts.ip)}`);
        this.redis = new Redis(opts.port || 6379,opts.ip || "127.0.0.1");
    }

    async get<T>(sid: string | number) {
        const data: any = await this.redis.get(`SESSION:${sid}`);
        return <T>JSON.parse(data);
    }

    async set(session, opts) {
        if(!opts.sid) {
            opts.sid = this.generateUID(24);
        }
        await this.redis.set(`SESSION:${opts.sid}`, JSON.stringify(session));
        return opts.sid;
    }

    async destroy(sid: string | number) {
        return await this.redis.del(`SESSION:${sid}`);
    }

    async socketSession(cookieStr: string) : Promise<Session> {
        if(cookieStr.includes('wp:sess')) {
            const sid: string = cookie.parse(cookieStr)['wp:sess'];
            if(!sid) return undefined; 
            const data : Session = await this.get<Session>(sid);
            return data;
        }
        return undefined;
    }

}

export interface sessionOptions extends cookies.IOptions {
    store?: Store;
    key?: string;
}

export function Koa_Session(opts: sessionOptions = {}) {
    opts.key = opts.key || "wp:sess";
    opts.store = opts.store || new LocalStore();

    return async function(ctx: any,next: any) : Promise<void> {
        let id = ctx.cookies.get(opts.key, opts);

        if(!id) {
            ctx.session = {};
        } 
        else {
            ctx.session = await opts.store.get(id);
            if(typeof ctx.session !== "object" || ctx.session == null) {
                ctx.session = {};
            }
        }

        const old: string = JSON.stringify(ctx.session);
        await next();
        if(old === JSON.stringify(ctx.session)) return;

        if(id) {
            await opts.store.destroy(id);
            id = null;
        }

        if(ctx.session && Object.keys(ctx.session).length) {
            let sid = await opts.store.set(ctx.session, Object.assign({}, opts, {sid: id}));
            ctx.cookies.set(opts.key, sid, opts);
        }
    }
}