import * as async from "async";
import * as path from "path";
import * as fs from "fs";

function getFiles(path: string) : Promise<string[]> {
    return new Promise<string[]>( (resolve,reject) => {
        fs.readdir(path,(err: NodeJS.ErrnoException,files: string[]) => {
            if(err) reject(err);
            resolve(files);
        });
    });
}

export async function Loader(pathStr: string | string[]) : Promise<string[]> {

    const finalLoad: string[] = [];
    const doLoad = async (pathStr: string) : Promise<void> => {
        let ext: string = path.extname(pathStr); 
        if(ext === '.js') {
            finalLoad.push(pathStr);
        }
        else {
            const files: string[] = await getFiles(pathStr);
            files.forEach( async (file: string) => {
                let file_ext: string = path.extname(file); 
                if(file_ext === '.js') 
                    finalLoad.push( path.join( pathStr, file ) );
            });
        }
    }

    if(typeof pathStr === "string") {
        await doLoad(pathStr);
        return finalLoad;
    }
    else if(pathStr instanceof Array) {
        pathStr.forEach( async (filepath: string) => {
            await doLoad(filepath);
        });
        return finalLoad;
    }
    else {
        return;
    }
}