/// <reference path='../../interfaces.d.ts'/>

import * as fs from "fs";
import * as path from "path";

export class Config {

    private static configPath: string = path.resolve( path.dirname(module.parent.filename) , "config.json" );
    private static defaultConfig: ConfigContent = {
        httpPort: 3500,
        database: 'localhost:27017/ethernuum',
        redis: {
            ip: '127.0.0.1',
            port: 6379
        }
    };
    private static configKeys: string[] = [];
    public static config: ConfigContent;

    /**
     * Get config value for a given key
     */
    public static get(key: string) {
        if(!~Config.configKeys.indexOf(key)) throw new Error(`Cannot find config under key ${key}`);

        const keys = key.split(".");
        let result = Config.config;

        for(let i = 0; i < keys.length; i++){
            result = result[keys[i]];
        }

        return result;
    }

    /**
     * Load config file
     */
    public static load() {
        Config.config = require(Config.configPath);
        Config.configKeys = Config.getKeys(Config.config);
    }

    /**
     * Check if config file exists and create it if not
     */
    public static init(configPath?: string,defaultConfig?: ConfigContent) {
        if(configPath) {
            this.configPath = configPath;
        }
        return new Promise<ConfigContent>(async (resolve: (obj: ConfigContent) => void) => {
            const exists: boolean = await Config.exists();
            if (exists) {
                this.load();
                return resolve(Config.config);
            }

            await Config.writeDefault(defaultConfig);
            console.log("Default configuration wrote, please personnalize.");
            process.exit(0);
        });
    }
    
    private static getKeys(obj: any, root: string = ""): string[] {
        let keys = [];
        Object.keys(obj).forEach(key => {
            const scannedObj = obj[key];
            keys[keys.length] = root + (root === "" ? "" : ".") + key;
            if (typeof scannedObj === "object" && !Array.isArray(scannedObj))
                keys = keys.concat(Config.getKeys(scannedObj, root + (root === "" ? "" : ".") + key));
        });

        return keys;
    }

    private static writeDefault(defaultConfig: ConfigContent) {
        return new Promise((resolve: () => void, reject: (err: Error) => void) => {
            fs.writeFile(Config.configPath, JSON.stringify(defaultConfig || Config.defaultConfig, null, 4), err => {
                err ? reject(err) : resolve();
            });
        });
    }

    private static exists() {
        return new Promise<boolean>((resolve: (exist: boolean) => void, reject: (err: Error) => void) => {
            fs.stat(Config.configPath, (err, stats) => {
                if (!err) resolve(true);
                else if (err.code === "ENOENT") {
                    resolve(false);
                } else {
                    reject(err);
                }
            });
        });
    }
}
