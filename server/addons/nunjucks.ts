import * as nunjucks from "nunjucks";
import * as path from "path";
import { readFile, existsSync } from "fs";

export interface templateOptions {
    cache?: boolean;
    globals?: Object;
}

function upStreamTemplate(templateName: string) : Promise<string> {
    return new Promise<string>( (resolve,reject) => {
        readFile(templateName,'utf8',(err: NodeJS.ErrnoException,data: string) => {
            if(err) throw new Error(err.toString());
            resolve(data);
        });
    });
}

export function KoaNunjucks(viewPath: string = 'views',opts: templateOptions = {}) {
    var compiledTemplates: { [key:string] : nunjucks.Template } = {};

    // Resolve path if not absolute ! 
    if(path.isAbsolute(viewPath) === false) {
        viewPath = path.resolve( path.dirname(module.parent.filename) , viewPath );
    }

    // Check if directory exist!
    if(existsSync(viewPath) === false) {
        throw new Error(`${viewPath} directory does'nt exist ! Please specify a correct directory!`);
    }

    return async function(ctx,next) : Promise<void> {
        ctx.render = async function(templateName: string,context: any) : Promise<void> {
            if(path.extname(templateName) === '') templateName = `${templateName}.html`;
            if(opts.globals !== undefined) {
                Object.assign(context,opts.globals);
            }
            if(opts.cache === true) {
                if(compiledTemplates.hasOwnProperty(templateName) === false) {
                    let templateString: string = await upStreamTemplate( path.join( viewPath, templateName ) );
                    compiledTemplates[templateName] = nunjucks.compile( templateString );
                }
                ctx.body = compiledTemplates[templateName].render(context);
            }
            else {
                const templateString: string = await upStreamTemplate( path.join( viewPath, templateName ) ); 
                ctx.body = nunjucks.renderString( templateString ,context);
            }
            
        }
        await next();
    }

}