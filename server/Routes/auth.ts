import * as router from "koa-router";
import * as passport from "passport";
import * as mongodb from "mongodb";
import toSHA256 from "../utils/password-hash";
import * as isEmail from 'validator/lib/isEmail';
const sendmail = require('sendmail')();

const defaultUser = {
    login: null,
    password: null,
    nickname: null,
    active: false,
    level: 0,
    exp: 0
}

function getRandomIntInRange(min = 0, max = 10) {
    [min, max] = [Math.min(min, max), Math.max(min, max)];
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function generateToken (length = 8, charset = undefined) {
    charset = charset || 'abcdefghijklmnopqrstuvwxyz0123456789';
    const min = 0;
    const max = charset.length - 1;

    let token = '';
    while(length) {
        token += charset.charAt(getRandomIntInRange(min, max))
        length--;
    }

    return token;
}

const AuthRouter: router = module.exports = new router({
    prefix: "/auth"
});

//AuthRouter.get('/facebook',passport.authenticate('facebook') as any);
//AuthRouter.get('/twitter',passport.authenticate('twitter') as any);

AuthRouter.post('/login', async (ctx: any,next) => {
    const login: string = ctx.request.body.email; 
    const password: string = ctx.request.body.password;

    if(login && password) {
        const user = await ctx.db.collection('users').findOne({login, password: toSHA256(password)});

        if(user) {
            if (user.active) {
                delete user.password;
                ctx.session.user = user;
                ctx.body = 'ok';
            }
            else {
                ctx.body = 'your account is not active, verify your mail';
            }
        }
        else {
            ctx.body = 'User not found';
        }
    }
    else {
        ctx.body = 'Invalid format';
    }
});

AuthRouter.post('/register', async (ctx:any,next) => {
    const login: string = ctx.request.body.email;
    const nickname: string = ctx.request.body.nickname;
    const password: string = ctx.request.body.password;
    const password2: string = ctx.request.body['password-2'];
    const token = generateToken(20);
    
    const errors = [];

    if (!(login && nickname && password && password2))
        errors.push('Invalid format');
    if (!isEmail(login))
        errors.push('Email bad format');
    if (password != password2)
        errors.push('Not identical password');
    if (password.length < 8)
        errors.push('Invaid password, minimum length : 8')

    let user = login ? await ctx.db.collection('users').findOne({login: login}) : null;
    if (user)
        errors.push('Username already exist');

    try {
        const result = !errors.length ? await ctx.db.collection('users').insertOne(Object.assign({}, defaultUser, {
            login,
            nickname,
            token,
            password: toSHA256(password)
        })) : null;

        // TODO : mieux gerer l'envoie de mail
        // TODO : prevoir un serveur SMTP et envoyer via le SMTP
        new Promise((resolve, reject) => {
            const url = `${ctx.request.origin}${AuthRouter.url('token', {token, login})}`;
            sendmail({
                from: 'ethernuum@ethernuum.com',
                to: login,
                subject: 'Valid your Ethernuum account',
                html: `Hello ${nickname}, here is your token link : <a href="${url}">${token}</a>`,
            }, function(err, reply) {
                if (err && err.stack) reject(err);
                else resolve(reply);
            });
        })
        .then(reply => console.log(reply))
        .catch(err => console.log(err))
    } catch(e) {
        errors.push('Database Error')
        console.log(e)
    }

    ctx.body = !errors.length ? 'Successfull, check your email !' : ` - ${errors.join('\n - ')}`;
});

AuthRouter.get('token', '/token/:token/:login', async (ctx: any, next) => {
    const {token, login} = ctx.params;
    const errors = [];

    if (!(token && login)) {
        errors.push('Invalid format')
    }

    try {
        let result = login ? await ctx.db.collection('users').findOneAndUpdate(
            {login, token}, // filter criteria
            {
                $set: {active: true}, // switch active to true
                $unset: {token} // delete token from document
            },
            {
                returnOriginal: false
            }
        ) : null;

        if (result) {
            delete result.value.password;
            ctx.session.user = result.value;
        }
    } catch (e) {
        errors.push('Database Error')
        console.log(e)
    }

    ctx.body = !errors.length ? 'Successfull, Account validated' : ` - ${errors.join('\n - ')}`;
})