import * as Router from "koa-router";
import * as path from 'path';
import * as fs from 'fs';
import * as chalk from "chalk";

const mimeTypes = {
    "html": "text/html",
    "jpeg": "image/jpeg",
    "jpg": "image/jpeg",
    "png": "image/png",
    "js": "text/javascript",
    "css": "text/css",
    "txt": "text/plain",
    "json": "application/json",
    "tag": "text/plain",
    "ttf": "font/opentype",
    "woff2": "font/woff2"
};

const ServeRouter: Router = module.exports = new Router({
    prefix: "/static"
});

ServeRouter.get('/:filepath*', async (ctx: any, next) => {
    const localPath : string = path.join(__dirname, '..', '..', 'static', ctx.params.filepath)
    ctx.response.type = mimeTypes[path.extname(localPath).split(".").pop()];
    console.log(chalk.green.bold('[Serving]')+' User requested asset '+chalk.bold.yellow(localPath)+', mime => '+chalk.cyan.bold(ctx.response.type));
    const stream: fs.ReadStream = fs.createReadStream(localPath)
    ctx.response.body = stream
});