import * as router from "koa-router";
import * as chalk from "chalk";

const GlobalRouter: router = module.exports = new router(); 

GlobalRouter.use( async(ctx:any,next) => {
    console.time(chalk.cyan.bold('routing_perf'))
    await next();
    console.log(chalk.green.bold('[Routing]')+' '+chalk.yellow.bold(ctx.url)+' requested !');
    console.timeEnd(chalk.cyan.bold('routing_perf'))
})

GlobalRouter.get('/play', async (ctx: any,next: () => any) => {
    await ctx.render('home/play');
});

GlobalRouter.get('/options', async (ctx: any,next: () => any) => {
    await ctx.render('home/options');
});

GlobalRouter.get('/universe', async (ctx: any,next: () => any) => {
    await ctx.render('home/universe');
});

GlobalRouter.get('/auth', async (ctx: any, next: () => any) => {
    await ctx.render('/home/auth');
});

GlobalRouter.get('/', async (ctx: any,next: () => any) => {
    await ctx.render('layout');
});