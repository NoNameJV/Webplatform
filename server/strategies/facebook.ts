import * as passport from "koa-passport";
import * as mongodb from "mongodb";
import * as facebook from "passport-facebook"; 

const Options: facebook.IStrategyOption = require('../../data/strategies.json').facebook;

export function FacebookStrategy(db: mongodb.Db) : facebook.Strategy {

    return new facebook.Strategy(Options, (accessToken: string, refreshToken: string, profile: facebook.Profile, done) => {
        process.nextTick( async () => {
            const usersCollection: mongodb.Collection = db.collection('users');
            const cursor: mongodb.Cursor = await usersCollection.find({facebook_id : profile.id })
            const users: any[] = await cursor.toArray(); 
            if(users.length > 0) {
                done(null,users[0]);
            }
            else {
                const newUser = {
                    facebook_id: profile.id,
                    facebook_token: accessToken,
                    username: `${profile.name.givenName} ${profile.name.familyName}`
                };

                const WriteResult: mongodb.InsertOneWriteOpResult = await usersCollection.insert(newUser);
                if(WriteResult.insertedCount == 1) {
                    done(null,newUser);
                }
                else {
                    done("Failed to insert new user!",false);
                }
            } 
        });
    });
}