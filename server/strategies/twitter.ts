import * as passport from "koa-passport";
import * as mongodb from "mongodb";
import * as twitter from "passport-twitter"; 

const Options: twitter.IStrategyOption = require('../../data/strategies.json').twitter;

export function TwitterStrategy(db: mongodb.Db) : twitter.Strategy {

    return new twitter.Strategy(Options, (accessToken: string, refreshToken: string, profile: twitter.Profile, done) => {
        process.nextTick( async () => {
            const usersCollection: mongodb.Collection = db.collection('users');
            const cursor: mongodb.Cursor = await usersCollection.find({ twitter_id : profile.id });
            const users: any[] = await cursor.toArray(); 
            if(users.length > 0) {
                done(null,users[0]);
            }
            else {
                const newUser = {
                    twitter_id: profile.id,
                    twitter_token: accessToken,
                    username: profile.displayName
                };

                const WriteResult: mongodb.InsertOneWriteOpResult = await usersCollection.insert(newUser);
                if(WriteResult.insertedCount == 1) {
                    done(null,newUser);
                }
                else {
                    done("Failed to insert new user!",false);
                }
            }
        });
    });
}