import * as crypto from "crypto";

function toSHA256(str: string) : string {
    return crypto.createHmac('sha256', 'random').update(str).digest('hex');
}

export default toSHA256;