import * as path from 'path';
import * as mongodb from "mongodb";
import * as chalk from "chalk";
import toSHA256 from "../utils/password-hash";

import { Config } from "../addons/config";

const users: IUser[] = require('../../data/users.json').users;

(async () => {
    const configuration: ConfigContent = await Config.init( path.join( __dirname , '..' , 'config.json' ) );
    mongodb.MongoClient.connect(`mongodb://${configuration.database}`, async (err: mongodb.MongoError, db: mongodb.Db) => {
        if(err) {
            console.log(chalk.red(err.toString()));
            return;
        }
        
        console.log(`${chalk.green.bold('[MongoDB]')} Connected with url ${chalk.yellow.bold(configuration.database)}`);
        console.log('Drop collections users & games');

        /*
        const p_games = db.dropCollection('games')
        p_games.then(isDrop => isDrop ? db.createCollection('games') : Promise.resolve(db.collection('games')))
            .then(dbGames => dbGames) // todo load games
            .catch(err => console.log(err.toString()))
        */

        db.listCollections({name: 'users'}).toArray()
            .then(collections => collections.length > 0)
            .then(exist => exist ? db.dropCollection('users') : false)
            .then(isDroped => db.createCollection('users'))
            .then(collection => {
                console.log('Hydrate users');
                users.forEach( async (user: IUser) => {
                    user.password = toSHA256(user.password);
                    collection.insert(user)
                        .then(write => {
                            if(write.insertedCount > 0) {
                                console.log(`${chalk.green.bold('Successfully')} inserted ${chalk.yellow.bold(user.login)} into users collection.`);
                            }
                            else {
                                console.log(`${chalk.red.bold('Failed')} to insert ${chalk.yellow.bold(user.login)} into users collection.`);
                            }
                            process.exit()
                        })
                        .catch(err => {
                            console.log(`${chalk.red.bold('Error')} to insert ${chalk.yellow.bold(user.login)} into users collection.`)
                            console.log(`${chalk.red.bold(err)}`)
                            process.exit(1)
                        })
                });
            })
            .catch(err => console.log(err.toString()))
    });

})();
