document.addEventListener('DOMContentLoaded',function() {

    var active_nav;
    var active_animation = false;
    var navigation = {
        'universe': function() {
        },
        'play': function() {
        },
        'options': function() {
            console.log('Options mounted!');
            riot.compile( function() {
                riot.mount('options-nav');
            } );
        },
        'auth': function() {
            console.log('Auth mounted!');
            riot.compile(function() {
                riot.mount('auth-register');
                riot.mount('auth-login');
            })
        }
    }

    function changeContent(url) {
        if(!navigation[url] || active_nav === url || active_animation) return;
        nav_content.classList.remove('show');
        active_animation = true;
        setTimeout(function() {
            var baseURI = window.location.hostname+window.location.port;
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4) {
                    if(xmlhttp.status == 200) {
                        window.location.hash = url;
                        active_nav = url;
                        nav_content.innerHTML = xmlhttp.responseText;
                        navigation[url]();
                    }
                    active_animation = false;
                    nav_content.classList.add('show');
                }
            }
            xmlhttp.open("GET", url);
            xmlhttp.setRequestHeader('Content-Type', 'utf8');
            xmlhttp.send();
        },360);
    }

    var nav_content = document.getElementById('nav_content');
    Object.getOwnPropertyNames(navigation).forEach(function(k) {
        let menu = document.getElementById('menu_'+k);
        menu.addEventListener('click', function() {
            changeContent(k);
        });
    })

    if(!window.location.hash) {
        changeContent('universe');
    }
    else {
        changeContent(window.location.hash.replace("#",""));
    }

});