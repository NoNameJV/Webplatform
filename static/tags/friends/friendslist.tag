<friendslist>

    <!-- HTML Layout -->
    <section class="friends hide">

    </section>
    <button id="open_friends">Open</button>

    <!-- CSS Style -->
    <style>
        @keyframes fadeIn {
            from { opacity: 0; }
            to { opacity: 1; }
        }

        @keyframes fadeOut {
            from { opacity: 1; }
            to { opacity: 0; }
        }

        button {
            height: 40px;
            padding: 0 10px;
        }

        section.friends {
            height: 600px;
            background: green;
            position: absolute;
            right: 25px;
            bottom: 60px;
            z-index: 999;
            width: 300px;
        }
            section.friends.show {
                display: flex;
                animation: fadeIn 150ms both;
                -webkit-animation: fadeIn 150ms both;
            }
            section.friends.hide {
                display: none;
                animation: fadeOut 150ms both;
                -webkit-animation: fadeOut 150ms both;
            }
    </style>

    <!-- JS Script -->
    <script>
        var visible = false;
        this.on('mount',function() {
            var friendsList = this.root.querySelector('.friends');

            this.root.querySelector('#open_friends').addEventListener('click',function(e) {
                console.log('click');
                if(visible) {
                    friendsList.classList.remove('show');
                    friendsList.classList.add('hide');
                }
                else {
                    friendsList.classList.remove('hide');
                    friendsList.classList.add('show');
                }
                visible = !visible;
            });
        });
    </script>

</friendslist>