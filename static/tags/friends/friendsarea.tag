<friendsarea>

    <!-- HTML Layout -->
    <tchat></tchat>
    <friendslist></friendslist>

    <!-- CSS Style -->
    <style>
        tchat {
            flex-grow: 1;
            position: relative;
            z-index: 1;
            height: 40px;
            align-self: flex-end;
            margin-right: 10px;
        }
        friendslist {
            background: #333;
            width: 400px;
            position: relative;
            z-index: 1;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 40px;
            align-self: flex-end;
        }
    </style>

    <!-- JS Script -->
    <script>
    </script>

</friendsarea>