<auth-login>
  <h1>Log in</h1>

  <form method="POST" action="/auth/login">
    <fieldset>
      <label for="email">E-mail</label>
      <input type="email" name="email" id="email" required/>

      <label for="password">password</label>
      <input type="password" name="password" id="password" required/>
    </fieldset>
    <button type="submit">Log in</button>
  </form>
</auth-login>