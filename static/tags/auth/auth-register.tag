<auth-register>
  <h1>Register</h1>

  <form method="POST" action="/auth/register">
    <fieldset>
      <label for="email">E-mail</label>
      <input type="email" name="email" id="email" required/>

      <label for="nickname">Nickname</label>
      <input type="text" name="nickname" id="nickname" required/>
    </fieldset>
    <fieldset>
      <label for="password">password</label>
      <input type="password" name="password" id="password" required/>

      <label for="password-2">repeat password</label>
      <input type="password" name="password-2" id="password-2" required/>
    </fieldset>
    <button type="submit">Register</button>
  </form>
</auth-register>