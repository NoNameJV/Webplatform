<user>

    <!-- HTML Layout -->
    <section class="main">
        <section class="top">
            <section class="avatar">
                <img src="/static/img/avatars/ow1.png" alt="avatar" />
            </section>
            <section class="info">
                <b class="name">Fraxken</b>
                <p class="level">Level<span>52</span></p>
                <p class="exp">2010/8500 exp</p>
            </section>
        </section>
        <section class="exp">
            <div class="on"></div>
            <div class="on"></div>
            <div class="on"></div>
            <div class="on"></div>
            <div class="on"></div>
            <div class="on"></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </section>
    </section>
    <ul class="menu">
        <li><i class="fa fa-trophy"></i></li>
        <li><i class="fa fa-sign-out"></i></li>
    </ul>

    <!-- CSS Style -->
    <style scoped>
        section.main {
            flex-grow: 1;
            display: flex;
            flex-direction: column;
        }
            section.main > section.top {
                flex-grow: 1;
                display: flex;
                margin-bottom: 5px;
            }
                section.main > section.top > section.avatar {
                    border: 2px solid #FFF;
                    border-radius: 2px;
                    overflow: hidden;
                    width: 85px;
                    height: 85px;
                    margin-right: 10px;
                    box-sizing: border-box;
                }
                    section.main > section.top > section.avatar img {
                        width: inherit;
                        height: inherit;
                    }

                section.main > section.top > section.info {
                    flex-grow: 1;
                    position: relative;
                    display: flex;
                    flex-direction: column;
                    height: 85px;
                    margin-right: 10px;
                }
                    section.main > section.top > section.info > b.name {
                        font-size: 26px;
                        color: #FFF;
                        font-family: "Exo";
                    }
                    section.main > section.top > section.info > p.exp {
                        color: #FFF;
                        position: absolute;
                        right: 0;
                        bottom: 0;
                        font-style: italic;
                        font-size: 14px;
                    }

            section.main > section.exp {
                height: 10px;
                display: flex;
                flex-shrink: 0;
                align-items: center;
                padding-right: 15px;
            }
                section.main > section.exp > div {
                    flex-grow: 1;
                    height: 6px;
                    border-radius: 2px;
                    background: #EEE;
                }
                    section.main > section.exp > div.on {
                        background: #EEFF41;
                    }
                    section.main > section.exp > div + div {
                        margin-left: 3px;
                    }

        ul.menu {
            width: 27px;
            flex-shrink: 0;
            display: flex;
            flex-direction: column;
            justify-content: flex-end;
            transform: skew(-6deg);
            margin-left: 6px;
        }
            ul.menu li {
                height: 27px;
                background: red;
                border-radius: 1px;
                display: flex;
                color: #FFF;
                align-items: center;
                justify-content: center;
            }
                ul.menu li + li {
                    margin-top: 8px;
                }

    </style>

    <!-- JS Script -->
    <script>

        add() {
            console.log('add action triggered');
        }

    </script>

</user>
