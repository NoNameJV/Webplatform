interface ConfigContent {
    httpPort: number;
    database: string;
    redis: {
        ip: string;
        port: number;
    }
}

interface Session {
    username: string;
}

interface IUser {
    login: string;
    password: string;
    level: number;
    exp: number;
}